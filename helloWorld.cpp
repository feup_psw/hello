// Software Design M.EEC FEUP

/*
To run, Terminal -> Run Task -> Windows or Linux
To debug, Run -> Start Debugging (F5)
*/

#include <stdio.h>
#include <iostream>   // std::cout
#include <string>     // std::string, std::to_string
using namespace std;  // after this clause, std is "defaulted", no need std::string, just use string, etc


int main()
{

    printf("Hello with pure C style... \n\r");


    string s="Test me ";  // this is c++, declaring a std::string variable

    cout << "Hello with CPP style: " << s << endl; 


    for(int i=0; i<5; i++) {
        s = s + to_string(i) + " " ;   // Press left of line number to define breakpoint and press F5 to use debugger
        cout << "CPP style ;) Loop Number:" << s << endl;
    }

    int i=-1;

    // see tasks.json, lines 9 and 30, "-Wall" will warn the situation below
    if (i=1) cout << "Careful: i=1 is atribution, not comparison" << endl;

    return(0);
}
