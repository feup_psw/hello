# hello
Written for the "Software Design" course of M.EEC, in FEUP, Portugal - Visual Studio Code Hello World in C and in C++ styles

Contributers: Armando Sousa and Ricardo Sousa

For Installation of Visual Studio Code in windows, see bottom of this file

## License
[GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)

In short, free software: Useful, transparent, no warranty


## What is it?
This very initial project lets you test Visual Studio Code under C++.

This project features:
- Printing a message in C style (with printf)
- Printing a message in c++ style (with << operator)
- Basic string usage (concatenation using std::string from C++)
- Advocates warnings as errors (includes forcing "-Wall" compiler flag)
- Encourages to use the debugger (small cycle that concatenates a std::string)

## Support video(s)
(Link will appear here...)

## Tests

Tested in VSC 1.61, windows, g++ (Rev5, Built by MSYS2 project) 10.3.0

Tested in VSC 1.61, linux,   g++ (Ubuntu 9.3.0-17ubuntu1~20.04) 9.3.0

The *.json files likely have dependencies to specific paths, either:

  1) adapt paths in the *.json files 

or 

  2) recreate the project keeping only hello_world.cpp

* Note: it is advocated to include "-Wall" in the compiler parameters


## Version / date
README edited in 2021/10/13



# Installation of Visual Studio Code (also in windows)

This section setsup the C/C++ development environment in Windows. 
This configuration is meant for the text editor Visual Studio Code - VS Code
(source: https://code.visualstudio.com/docs/languages/cpp).

Start by downloading and installing VSCode.

## Install the C/C++ extension (all OS)

1. Open VS Code
2. Select the Extensions view on the Activity bar (left-side ribbon) or use the
   keyboard shortcut (`Ctrl+Shift+X`) or use the menu (View > Extensions)
3. Search for `'C++'`
4. Install the following extensions:
   - C/C++ by Microsoft
   - C/C++ Extension Pack by Microsoft (optional)

## Install a compiler (MinGW-x64) -- in other OS the compilers are native...

If you are on windows, then you need to install MSYS

1. Go to [MSYS2](https://www.msys2.org/)
2. Download the installer `msys2-x86_64....exe`
3. Run the installer
4. Enter your desired Installation Folder (**Note:** ASCII-only, no accents, no
   spaces)
5. Run MSYS2 64bit and update the package database and base packages:

```sh
# Open MSYS2 MSYS
pacman -Syu
# Open MSYS2 MSYS from the start menu
pacman -Su
```

6. Install some tools and the mingw-w64 GCC to start compiling:

```sh
pacman -S --needed base-devel mingw-w64-x86_64-toolchain
# Enter 2 times for default option 'all' and then Y
```

## Configure the MinGW compiler (on windows)

Add the path of your MinGW-w64 `bin` folder to the Windows `PATH` environment
variable by using the following steps:

1. Windows search bar `'settings'`
2. Search `'Edit environment variables for your account'`
3. Choose the `Path` variable and the select Edit
4. Select New and add the MinGW-w64 destination folder path, with `\mingw64\bin`
   appended. The path depends on which version of MinGW-w64 you have installed
   it. If you use the settings above to install MinGW-64, then add this to the
   path: `C:\msys64\mingw64\bin`
5. Select OK to save the updated `PATH`. You need to reopen any consele for the
   new `PATH` location to be available


To see current path and confirm settings, you can open a command prompt window
by pressing `windows+X` and selecting `command prompt`, then write `path` and
analyse the output that should include something like:
`C:\VSCode\bin;C:\msys64\mingw64\bin;C:\msys64\usr\bin;`

Note that if all paths are well configured, CMD, power shell and MSYS_Bash should work.


## Check if your MinGW installation is correctly installed

Open a New command prompt (e.g., in VS Code, Terminal > New Terminal - make sure
that it is a command prompt and not a bash terminal):

[The `$` is not no be written, it is command prompt]

```sh
$ g++ --version
g++ (Rev5, Built by MSYS2 project) 10.3.0
Copyright (C) 2020 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

$ gdb --version
GNU gdb (GDB) 10.2
Copyright (C) 2021 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```

## Check if your MinGW installation is installed and configured correctly in VS Code

1. Open the hello example
2. Build helloWorld.cpp
   - Terminal > Run Build Task
   - C/C++: g++.exe build active file
   - Check if a `helloWorld.exe` file was created
3. Run HelloWorld
   - Open Command Prompt (Terminal > New Terminal)
   - Type `.\helloworld` and press enter
   - Output expected:

```sh
Hello with pure C style...
Hello with CPP style: Test me
CPP style ;) Loop Number:Test me 0
CPP style ;) Loop Number:Test me 0 1
CPP style ;) Loop Number:Test me 0 1 2
CPP style ;) Loop Number:Test me 0 1 2 3
CPP style ;) Loop Number:Test me 0 1 2 3 4
Careful: i=1 is atribution, not comparison
```


## Check if your MinGW installation is installed and configured correctly in the terminal inside VS Code

Go to the Terminal menu and open a new terminal, on windows, press `CTRL+ç`.

Most likely, on windows VSCode, a PowerShell terminal will open.

On any terminal, write:
```sh
 g++ ./helloWorld.cpp -o ./helloWorld.exe
```

**Note**: Windows uses \ backslashes and linux use / forwardslashes

No errors!

Now try:
```sh
 g++ -Wall .\helloWorld.cpp -o .\helloWorld.exe
```

This will produce a nice **warning** message

To execute, write `./helloWorld` and the output will be something like:
````
Hello with pure C style...
Hello with CPP style: Test me
CPP style ;) Loop Number:Test me 0
CPP style ;) Loop Number:Test me 0 1
CPP style ;) Loop Number:Test me 0 1 2
CPP style ;) Loop Number:Test me 0 1 2 3
CPP style ;) Loop Number:Test me 0 1 2 3 4
Careful: i=1 is atribution, not comparison
````

## Use the visual debugger of the IDE

Go to the helloWorld.cpp file, place a breakpoint at the left of line 27 and press F5.


## Additional information and Download Link

Google is your friend :) ... 

VSCode and GCC/G++ are widely used and there are many introductory resources and videos.

Here is the link for the [VSCode download and documentation](https://code.visualstudio.com/docs)

